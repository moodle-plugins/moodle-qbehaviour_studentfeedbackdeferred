<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbehaviour_studentfeedbackdeferred', language 'fr'
 * @package    qbehaviour_studentfeedbackdeferred
 * @copyright  2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['behavioursummary'] = 'Commentaire';
$string['cannotsubmitfeedback'] = 'Vous ne pouvez pas envoyer de commentaire ici. Cette tentative ne vous appartient pas, ou bien vous n\'avez pas les permissions requises.';
$string['eventstudentgeneralfeedbackupdated'] = 'Commentaire de l\'étudiant mis à jour pour le test';
$string['eventstudentquestionfeedbackupdated'] = 'Commentaire de l\'étudiant mis à jour pour la question';
$string['invaliddataorsesskey'] = 'Cette action a été interrompue pour des raisons de sécurité. Les données n\'ont pas été envoyées correctement, ou bien votre clé de session n\'a pas pu être validée.';
$string['pluginname'] = 'Retour de l\'étudiant (différé)';
$string['studentsfeedback'] = 'Commentaire de l\'étudiant à propos de sa réponse :';
$string['studentsgeneralfeedback'] = 'Commentaire de l\'étudiant à propos de ses résultats dans leur ensemble :';
$string['yourfeedback'] = 'Votre commentaire à propos de votre réponse :';
$string['yourgeneralfeedback'] = 'Votre commentaire à propos de vos résultats dans leur ensemble :';
