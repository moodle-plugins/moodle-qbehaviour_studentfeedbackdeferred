<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qbehaviour_studentfeedbackdeferred', language 'en'
 * @package    qbehaviour_studentfeedbackdeferred
 * @copyright  2021 Astor Bizard <astor.bizard@univ-grenoble-alpes.fr>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['behavioursummary'] = 'Comment';
$string['cannotsubmitfeedback'] = 'You can not submit feedback here. Either this attempt does not belong to you, or you do not have permissions.';
$string['eventstudentgeneralfeedbackupdated'] = 'Student feedback for quiz updated';
$string['eventstudentquestionfeedbackupdated'] = 'Student feedback for question updated';
$string['invaliddataorsesskey'] = 'This action has been prevented for security reasons. Either data has not been submitted properly, or you session key could not be confirmed.';
$string['pluginname'] = 'Student feedback (deferred)';
$string['privacy:behaviourpath'] = 'Behaviour';
$string['privacy:metadata:attemptstepdata'] = 'This plugin stores some data submitted during quiz attempts.';
$string['privacy:metadata:name'] = 'Attempt step data variable name, among [studentfeedback, generalstudentfeedback].';
$string['privacy:metadata:value'] = 'Attempt step data variable value.
For studentfeedback, feedback submitted after a question attempt, for each attempted question using this behaviour.
For generalstudentfeedback, global feedback submitted after a quiz attempt, for each quiz using this behaviour.';
$string['studentsfeedback'] = 'Student\'s comment about their answer:';
$string['studentsgeneralfeedback'] = 'Student\'s comment about their results:';
$string['yourfeedback'] = 'Your comment about your answer:';
$string['yourgeneralfeedback'] = 'Your comment about your results as a whole:';
